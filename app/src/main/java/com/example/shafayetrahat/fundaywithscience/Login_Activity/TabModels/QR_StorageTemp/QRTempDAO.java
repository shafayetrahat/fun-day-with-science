package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.QR_StorageTemp;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface QRTempDAO {

    @Insert
    void insertTask(QRTempEntity qrTempEntity);

    @Query("DELETE FROM qrCode")
    void deleteAll();

    @Query("SELECT * from qrCode ORDER BY id ASC")
    LiveData<List<QRTempEntity>> getAllQRTempEntity();
}


