package com.example.shafayetrahat.fundaywithscience.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Share_Coins {

    @SerializedName("reciever")
    @Expose
    private String reciever;
    @SerializedName("sender")
    @Expose
    private String sender;

    public String getReciever() {
        return reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

}
