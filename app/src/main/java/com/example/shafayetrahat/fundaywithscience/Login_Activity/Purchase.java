package com.example.shafayetrahat.fundaywithscience.Login_Activity;

import com.example.shafayetrahat.fundaywithscience.Models.CartBody;
import com.example.shafayetrahat.fundaywithscience.Network.Network;
import com.example.shafayetrahat.fundaywithscience.RouteInterface.RouteInterface;
import com.example.shafayetrahat.fundaywithscience.Shared_Preferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Purchase {
    List<String> itemName;
    List<Integer> itemAmount;
    int netPrice;


    Purchase(List<String> itemName, List<Integer>itemAmount, int netPrice){
        this.itemName = itemName;
        this.itemAmount = itemAmount;
        this.netPrice = netPrice;
    }

}
