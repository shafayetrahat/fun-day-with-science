package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.QR_StorageTemp.QRRepo;
import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.QR_StorageTemp.QRTempEntity;
import com.example.shafayetrahat.fundaywithscience.Models.ShareCoinsBody;
import com.example.shafayetrahat.fundaywithscience.Models.Share_Coins;
import com.example.shafayetrahat.fundaywithscience.Network.Network;
import com.example.shafayetrahat.fundaywithscience.R;
import com.example.shafayetrahat.fundaywithscience.RouteInterface.RouteInterface;
import com.example.shafayetrahat.fundaywithscience.Shared_Preferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShareCoinsFinal extends AppCompatActivity {
    private RouteInterface routeShareCoins;
    ShareCoinsBody shareCoinsBody = new ShareCoinsBody();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_coins_final);
        final Button confirm = findViewById(R.id.scan_share_coin);
        setCoinsBody();
        confirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                shareCoins();
            }
        });
    }

    public void setCoinsBody(){
        QRRepo qrRepo = new QRRepo(getApplicationContext());
        qrRepo.getAllqrTempEntity().observe(ShareCoinsFinal.this, new Observer<List<QRTempEntity>>() {
            @Override
            public void onChanged(List<QRTempEntity> QRTempEntities) {
                int length = 0;
                for(QRTempEntity qrTempEntity : QRTempEntities) {
//                    System.out.println("#############"+qrTempEntity.qrCode);
                    if (length == 0){
                        String s_qr = qrTempEntity.qrCode;
                        shareCoinsBody.setSenderQrCode(s_qr);
                    }
                    if (length==1){
                        String r_qr = qrTempEntity.qrCode;
                        shareCoinsBody.setRecieverQrCode(r_qr);
                    }
                    length++;

                }
            }
        });



    }

    public void shareCoins(){
//        System.out.println("******************"+shareCoinsBody.getSenderQrCode());
//        System.out.println("******************"+shareCoinsBody.getRecieverQrCode());
//        System.out.println("***************"+shareCoinsBody.getamount());
        EditText edit = findViewById(R.id.coins);
        String coins_t = edit.getText().toString();
        int coins = Integer.parseInt(coins_t);

        shareCoinsBody.setamount(coins);
        routeShareCoins = Network.getClient().create(RouteInterface.class);
        String token = Shared_Preferences.getSharedPreferenceString(this,"token","");
        Call<Share_Coins> call = routeShareCoins.transferCoins("Bearer "+token, shareCoinsBody);
        call.enqueue(new Callback<Share_Coins>() {
            @Override
            public void onResponse(Call<Share_Coins> call, Response<Share_Coins> response) {
                Share_Coins shareCoins = response.body();
                String detailsUser = shareCoins.getSender();
                    try {
                        QRRepo qrcode = new QRRepo(getApplicationContext());
                        qrcode.deleteAll();
                        Intent k = new Intent(ShareCoinsFinal.this, ShareCoins.class);
                        k.putExtra("flash", "Transfer successfull");
                        startActivity(k);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            @Override
            public void onFailure(Call<Share_Coins> call, Throwable t) {

                Toast.makeText(ShareCoinsFinal.this,"Error in network",Toast.LENGTH_LONG).show();

            }

        });
    }
}
