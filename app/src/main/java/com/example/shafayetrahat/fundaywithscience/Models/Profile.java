package com.example.shafayetrahat.fundaywithscience.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile {

    @SerializedName("earn_coin")
    @Expose
    private Integer earnCoin;
    @SerializedName("spent_coin")
    @Expose
    private Integer spentCoin;
    @SerializedName("net_coin")
    @Expose
    private Integer netCoin;
    @SerializedName("user")
    @Expose
    private String user;

    public Integer getEarnCoin() {
        return earnCoin;
    }

    public void setEarnCoin(Integer earnCoin) {
        this.earnCoin = earnCoin;
    }

    public Integer getSpentCoin() {
        return spentCoin;
    }

    public void setSpentCoin(Integer spentCoin) {
        this.spentCoin = spentCoin;
    }

    public Integer getNetCoin() {
        return netCoin;
    }

    public void setNetCoin(Integer netCoin) {
        this.netCoin = netCoin;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
