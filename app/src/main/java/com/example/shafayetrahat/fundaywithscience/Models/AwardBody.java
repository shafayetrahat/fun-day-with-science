package com.example.shafayetrahat.fundaywithscience.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AwardBody {

    @SerializedName("qr_code")
    @Expose
    private String qrCode;
    @SerializedName("event_id")
    @Expose
    private Integer eventId;

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

}