package com.example.shafayetrahat.fundaywithscience;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shafayetrahat.fundaywithscience.Login_Activity.Panel_Page;
import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.QR_StorageTemp.QRRepo;
import com.example.shafayetrahat.fundaywithscience.Models.Login;
import com.example.shafayetrahat.fundaywithscience.Models.Login_Body;
import com.example.shafayetrahat.fundaywithscience.Network.Network;
import com.example.shafayetrahat.fundaywithscience.RouteInterface.RouteInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.Manifest;

import java.util.ArrayList;
import java.util.List;


public class MainPage extends AppCompatActivity {
    private RouteInterface route_list;
    private static final int PERMISSION_CODE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        QRRepo qrcode = new QRRepo(getApplicationContext());
        qrcode.deleteAll();
        checkAndRequestPermissions();
        route_list = Network.getClient().create(RouteInterface.class);
        final Button login = findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                login_Activity();
            }
        });

    }


    private  boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.INTERNET);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),PERMISSION_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void login_Activity()
    {

        EditText email = findViewById(R.id.username);
        EditText password = findViewById(R.id.password);
        String s_password = password.getText().toString();
        String s_email = email.getText().toString();
        Login_Body login_body = new Login_Body();
        login_body.setEmail(s_email);
        login_body.setPassword(s_password);
//        Log.d("tag",login_body.getEmail());
        Validity(login_body);
//        Toast.makeText(MainPage.this,login_body.getEmail(),Toast.LENGTH_SHORT).show();


    }
    private void Validity (Login_Body credential)
    {
        Call<Login> call = route_list.userLogin(credential);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Login validity = response.body();
                String token = validity.getSuccess().getToken().toString();
                Shared_Preferences.setSharedPreferenceString(MainPage.this,"token",token);
                if(token!=null){
                    try {
                        Intent k = new Intent(MainPage.this, Panel_Page.class);
                        startActivity(k);
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Toast.makeText(MainPage.this,"Error in Login",Toast.LENGTH_LONG).show();
            }
        });
    }

}
