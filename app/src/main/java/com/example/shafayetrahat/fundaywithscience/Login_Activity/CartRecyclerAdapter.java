package com.example.shafayetrahat.fundaywithscience.Login_Activity;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
//import android.widget.EditText;
import android.widget.TextView;

import com.example.shafayetrahat.fundaywithscience.R;

import java.util.ArrayList;
import java.util.List;

public class CartRecyclerAdapter extends RecyclerView.Adapter<CartRecyclerAdapter.ViewHolder> {

    private List<String> productName;
    private List<Integer> product_Price;
    private LayoutInflater mInflater;
    public List<Integer> tempAmount;
    private ItemClickListener mClickListener;

//    public LiveData<Integer> netPrice;


    // data is passed into the constructor
    CartRecyclerAdapter(Context context, List<String> data, List<Integer> price, List<Integer> tempAmount) {
        this.mInflater = LayoutInflater.from(context);
        this.productName = data;
        this.product_Price = price;
        this.tempAmount = tempAmount;

    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.cart_recyler_view, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String Name = productName.get(position);
        holder.productName.setText(Name);
        String price = product_Price.get(position).toString();
        holder.productPrice.setText(price+" TK");
        holder.productAmount.setText(String.valueOf(tempAmount.get(position)));
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return productName.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView productName;
        TextView productPrice;
        TextView productAmount;
        Button increase;
        Button decrease;
        ViewHolder(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.productName);
            productPrice = itemView.findViewById(R.id.productPrice);
            productAmount = itemView.findViewById(R.id.productAmount);
            increase = itemView.findViewById(R.id.increaseItem);
            decrease = itemView.findViewById(R.id.decreaseItem);
            increase.setOnClickListener(this);
            decrease.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.increaseItem:
                    mClickListener.onIncreaseClick(view, this.getLayoutPosition());
                    break;
                case R.id.decreaseItem:
                    mClickListener.onDecreaseClick(view,this.getLayoutPosition());
                    break;
                default:
                    break;
            }
        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return productName.get(id);
    }
    Integer getprice(int id) {
        return product_Price.get(id);
    }
    Integer getTempAmount(int id) {
        return tempAmount.get(id);
    }


    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onIncreaseClick(View view, int position);
        void onDecreaseClick(View view, int position);
    }
}