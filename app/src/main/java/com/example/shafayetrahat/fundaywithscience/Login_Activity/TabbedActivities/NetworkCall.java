package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities;

import android.os.AsyncTask;

import com.example.shafayetrahat.fundaywithscience.Models.Award;
import com.example.shafayetrahat.fundaywithscience.Models.AwardBody;
import com.example.shafayetrahat.fundaywithscience.Network.Network;
import com.example.shafayetrahat.fundaywithscience.RouteInterface.RouteInterface;
import com.example.shafayetrahat.fundaywithscience.Shared_Preferences;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

public abstract class NetworkCall extends AsyncTask<Call, Void, String> {
    private RouteInterface route_profile;

    protected String doInBackground(String token, AwardBody awardBody) {
        try {
            route_profile = Network.getClient().create(RouteInterface.class);
            Call <Award> call = route_profile.postAward("Bearer" + token, awardBody);
            Response<Award> response = call.execute();
            return response.body().toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

