package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.shafayetrahat.fundaywithscience.Models.Award;
import com.example.shafayetrahat.fundaywithscience.Models.AwardBody;
import com.example.shafayetrahat.fundaywithscience.Network.Network;
import com.example.shafayetrahat.fundaywithscience.R;
import com.example.shafayetrahat.fundaywithscience.RouteInterface.RouteInterface;
import com.example.shafayetrahat.fundaywithscience.barcodeUtil.BarcodeCaptureActivity;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.example.shafayetrahat.fundaywithscience.Shared_Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GamesConfirm extends AppCompatActivity {
    public static final int RC_BARCODE_CAPTURE = 9001;
    AwardBody awardBody = new AwardBody();
    private RouteInterface route_profile;
    private ProgressBar pgsBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games_confirm);
        pgsBar = (ProgressBar) findViewById(R.id.progressBar);
        showQRScanner();
    }
    public void showQRScanner() {
        Intent intent = new Intent(GamesConfirm.this, BarcodeCaptureActivity.class);
        startActivityForResult(intent, RC_BARCODE_CAPTURE);
        return;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == CommonStatusCodes.SUCCESS && requestCode == RC_BARCODE_CAPTURE) {
            if (data == null) return;
            Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
            final String scanResult = barcode.displayValue;
            if (scanResult == null) return;
            Integer event_id = Shared_Preferences.getSharedPreferenceInt(this,"event_id",0);
//            System.out.println("xxxxxxx"+event_id+"######"+scanResult);
            awardBody.setEventId(event_id);
            awardBody.setQrCode(scanResult);
            eventsReward();
            try {
                Thread.sleep(1000);
                pgsBar.setVisibility(View.GONE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

//            result = findViewById(R.id.qr_token);
////            result.setText(scanResult);
//            Profile_body profilebody = new Profile_body();
//            profilebody.setQrCode(scanResult);
//            showProfile(profilebody);


        }
    }
    public void eventsReward(){
        route_profile = Network.getClient().create(RouteInterface.class);
        String token = Shared_Preferences.getSharedPreferenceString(this,"token","");
        Call <Award> call = route_profile.postAward("Bearer "+token, awardBody);
        pgsBar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<Award>() {
            @Override
            public void onResponse(Call<Award> call, Response<Award> response) {
//                progressDoalog.dismiss();
                Award status = response.body();
                String status_report = status.getStatus();
                    try {

                        Toast.makeText(GamesConfirm.this,"Transaction "+status_report,Toast.LENGTH_LONG).show();
                        System.out.println("Transaction "+status_report);
                    } catch(Exception e) {
                        Toast.makeText(GamesConfirm.this,"Transaction code error but your transaction is complete. Plz check you balance",Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }

            }
            @Override
            public void onFailure(Call<Award> call, Throwable t) {
//                progressDoalog.dismiss();
                pgsBar.setVisibility(View.GONE);
                Toast.makeText(GamesConfirm.this,"Error in network or transaction unsuccessful.Plz try again",Toast.LENGTH_LONG).show();
            }
        });
        Shared_Preferences.deleteSharedPreference(this,"event_id");

    }
}