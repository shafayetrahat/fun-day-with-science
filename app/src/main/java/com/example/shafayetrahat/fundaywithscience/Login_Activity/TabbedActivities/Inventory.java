package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.shafayetrahat.fundaywithscience.Login_Activity.Cart_Page;
import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.CartTemp.CartRepo;
import com.example.shafayetrahat.fundaywithscience.Models.InventoryModel;
import com.example.shafayetrahat.fundaywithscience.Models.Item;
import com.example.shafayetrahat.fundaywithscience.Network.Network;
import com.example.shafayetrahat.fundaywithscience.R;
import com.example.shafayetrahat.fundaywithscience.RouteInterface.RouteInterface;
import com.example.shafayetrahat.fundaywithscience.Shared_Preferences;
import com.example.shafayetrahat.fundaywithscience.barcodeUtil.BarcodeCaptureActivity;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Inventory extends AppCompatActivity implements InventoryRecyclerAdapter.ItemClickListener{
    InventoryRecyclerAdapter adapter;
    private RouteInterface routeInventory;
//    private ProgressBar pgsBar;
    ArrayList<String> itemName = new ArrayList<>();
    ArrayList<Integer> itemPrice = new ArrayList<>();
    ArrayList<Integer> itemAmount = new ArrayList<>();
    private String qrCode= null;
    public static final int RC_BARCODE_CAPTURE = 9001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);
        inventoryList();
        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }



        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.inventory);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        adapter = new InventoryRecyclerAdapter(this,itemName, itemPrice, itemAmount );
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        qrCode= Shared_Preferences.getSharedPreferenceString(this,"qrCode","");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(qrCode==null||qrCode=="") {
                    fabButtonClick();
                }
                else {
                    Intent intent = new Intent(Inventory.this, Cart_Page.class);
                    startActivity(intent);
                }

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "Added " + adapter.getItem(position) + " to cart, item number " + position, Toast.LENGTH_SHORT).show();
        CartRepo cartRepo = new CartRepo(getApplicationContext());
        cartRepo.insertTask(adapter.getItem(position), adapter.getprice(position), adapter.getavailability(position));

    }
    public void inventoryList(){
        routeInventory = Network.getClient().create(RouteInterface.class);
        String token = Shared_Preferences.getSharedPreferenceString(this,"token","");
        Call<InventoryModel> call = routeInventory.getInventoryList("Bearer "+token);
        call.enqueue(new Callback<InventoryModel>(){
            InventoryModel inventoryModel = null;
            @Override
            public void onResponse(Call<InventoryModel> call, Response<InventoryModel> response) {
                try {
                        inventoryModel = response.body();
                    for(Item inventoryItem : inventoryModel.getItems()) {
                        itemName.add(inventoryItem.getItem());
                        itemPrice.add(inventoryItem.getPrice());
                        itemAmount.add(inventoryItem.getAmount());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<InventoryModel> call, Throwable t) {
                Toast.makeText(Inventory.this,"Error in network",Toast.LENGTH_LONG).show();

            }

        });
    }
    public void fabButtonClick(){
        showQRScanner();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CommonStatusCodes.SUCCESS && requestCode == RC_BARCODE_CAPTURE) {
            if (data == null) return;
            Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
            final String scanResult = barcode.displayValue;
            if (scanResult == null) return;
            Shared_Preferences.setSharedPreferenceString(getApplicationContext(),"qrCode",scanResult);
            if (scanResult!=null){
                Intent intent = new Intent(Inventory.this, Cart_Page.class);
                startActivity(intent);
            }



        }
    }
    public void showQRScanner() {
        Intent intent = new Intent(Inventory.this, BarcodeCaptureActivity.class);
        startActivityForResult(intent, RC_BARCODE_CAPTURE);
        return;
    }
}
