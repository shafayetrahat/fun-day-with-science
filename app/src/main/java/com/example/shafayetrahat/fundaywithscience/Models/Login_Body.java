
package com.example.shafayetrahat.fundaywithscience.Models; ;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login_Body {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

}