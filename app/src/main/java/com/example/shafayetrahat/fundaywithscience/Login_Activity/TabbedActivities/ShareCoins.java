package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.QR_StorageTemp.QRRepo;
import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.QR_StorageTemp.QRTempEntity;
import com.example.shafayetrahat.fundaywithscience.Models.ShareCoinsBody;
import com.example.shafayetrahat.fundaywithscience.Models.Share_Coins;
import com.example.shafayetrahat.fundaywithscience.Network.Network;
import com.example.shafayetrahat.fundaywithscience.R;
import com.example.shafayetrahat.fundaywithscience.RouteInterface.RouteInterface;
import com.example.shafayetrahat.fundaywithscience.Shared_Preferences;
import com.example.shafayetrahat.fundaywithscience.barcodeUtil.BarcodeCaptureActivity;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.shafayetrahat.fundaywithscience.R.id.coins;

public class ShareCoins extends AppCompatActivity {
    public int length= 0;
    public static final int RC_BARCODE_CAPTURE = 9001;
    String flashmsg = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_coins);
         final Button scanButton = findViewById(R.id.scan_share_coin);
         flashmsg = getIntent().getStringExtra("flash");
        Toast.makeText(ShareCoins.this,flashmsg,Toast.LENGTH_LONG).show();
        scanButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    QRScanner();
                }
            });

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == CommonStatusCodes.SUCCESS && requestCode == RC_BARCODE_CAPTURE) {
            if (data == null) return;
            Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
            final String scanResult = barcode.displayValue;
            if (scanResult == null) return;
            storeQRScanner(scanResult);



        }
    }
    public void storeQRScanner(String scanResult){
        QRRepo qrcode = new QRRepo(getApplicationContext());
        qrcode.insertTask(scanResult);
        qrcode.getAllqrTempEntity().observe(ShareCoins.this, new Observer<List<QRTempEntity>>() {
            @Override
            public void onChanged(List<QRTempEntity> QRTempEntities) {
                for(QRTempEntity qrTempEntity : QRTempEntities) {
                    length++;
                    System.out.println(qrTempEntity.id+"--++++--"+qrTempEntity.qrCode);
                }
            }
        });
        if (length == 1){
            TextView textView = findViewById(R.id.qr_token_share_coin);
            textView.setText("Scan successful");
            Intent k = new Intent(ShareCoins.this, ShareCoinsFinal.class);
            startActivity(k);
        }
        else {
            TextView textView = findViewById(R.id.qr_token_share_coin);
            textView.setText("Now scan for receiver's qr code");
        }
    }
    public void QRScanner() {
        Intent intent = new Intent(ShareCoins.this, BarcodeCaptureActivity.class);
        startActivityForResult(intent, RC_BARCODE_CAPTURE);
        return;
    }


}