package com.example.shafayetrahat.fundaywithscience.Login_Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.shafayetrahat.fundaywithscience.GridviewAdapter;
import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.QR_StorageTemp.QRRepo;
import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities.CheckBalance;
import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities.ShareCoins;
import com.example.shafayetrahat.fundaywithscience.R;
import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities.Games;
import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities.Inventory;
import com.example.shafayetrahat.fundaywithscience.Shared_Preferences;
import com.example.shafayetrahat.fundaywithscience.barcodeUtil.BarcodeCaptureActivity;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

public class Panel_Page extends AppCompatActivity {
    public static final int RC_BARCODE_CAPTURE = 9001;
    public static final int REQUEST_CODE_PERMISSION = 1001;
    private String qrCode= null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel__page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        qrCode= Shared_Preferences.getSharedPreferenceString(this,"qrCode","");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(qrCode==""||qrCode==null) {
                    fabClickListener();
                }
                else {
                    Intent intent = new Intent(Panel_Page.this, Cart_Page.class);
                    startActivity(intent);
                }
            }
        });
        ////////////////////////////////////////////
        GridView grid;
        final String[] web = {
                "Check Balance",
                "Events",
                "Inventory",
                "Share Coins"

        } ;
        int[] imageId = {
                R.drawable.ic_coins_icon,
                R.drawable.ic_controller,
                R.drawable.ic_shop,
                R.drawable.ic_share_coins
        };

            GridviewAdapter adapter = new GridviewAdapter(Panel_Page.this, web, imageId);
            grid=(GridView)findViewById(R.id.gridview);
            grid.setAdapter(adapter);
            grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
//                    Toast.makeText(Panel_Page.this, "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();
                    if(position==0){
                        try {
                            Intent intent = new Intent(Panel_Page.this, CheckBalance.class);
                            startActivity(intent);
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (position==1){
                        try {
                            Intent k = new Intent(Panel_Page.this, Games.class);
                            startActivity(k);
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (position==2){
                        try {
                            Intent k = new Intent(Panel_Page.this, Inventory.class);
                            startActivity(k);
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (position==3){
                        try {
                            Intent k = new Intent(Panel_Page.this, ShareCoins.class);
                            startActivity(k);
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                    }


                }
            });

        }
        ////////////////////////////////////////////
    public void fabClickListener(){
        showQRScanner();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CommonStatusCodes.SUCCESS && requestCode == RC_BARCODE_CAPTURE) {
            if (data == null) return;
            Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
            final String scanResult = barcode.displayValue;
            if (scanResult == null) return;
            Shared_Preferences.setSharedPreferenceString(getApplicationContext(),"qrCode",scanResult);
            if (scanResult!=null){
                Intent intent = new Intent(Panel_Page.this, Cart_Page.class);
                startActivity(intent);
            }

        }
    }
    public void showQRScanner() {
        Intent intent = new Intent(Panel_Page.this, BarcodeCaptureActivity.class);
        startActivityForResult(intent, RC_BARCODE_CAPTURE);
        return;
    }


}
