package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.QR_StorageTemp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity(tableName = "qrCode")
public class QRTempEntity {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "qrCode")
    public String qrCode;
}

