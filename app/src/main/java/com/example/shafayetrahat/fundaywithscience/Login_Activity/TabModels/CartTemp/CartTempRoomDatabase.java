package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.CartTemp;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;


@Database(entities = {CartTempEntity.class}, version = 2, exportSchema = false)
public abstract class CartTempRoomDatabase extends RoomDatabase {

    public abstract CartTempDAO cartTempDAO();
}