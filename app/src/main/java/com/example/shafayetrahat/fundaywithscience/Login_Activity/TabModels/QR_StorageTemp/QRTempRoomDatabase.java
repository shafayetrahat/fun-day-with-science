package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.QR_StorageTemp;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


@Database(entities = {QRTempEntity.class}, version = 1, exportSchema = false)
public abstract class QRTempRoomDatabase extends RoomDatabase {

    public abstract QRTempDAO qrTempDAO();
}