package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.CartTemp;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;


public class CartRepo {
    private String DB_NAME = "TempDatabase";

    private static CartTempRoomDatabase cartTempRoomDatabase;
    public CartRepo(Context context) {
        cartTempRoomDatabase = Room.databaseBuilder(context, CartTempRoomDatabase.class, DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    public void insertTask(String product, Integer price, Integer availability) {

        CartTempEntity cartTempEntity = new CartTempEntity();
        cartTempEntity.product = product;
        cartTempEntity.price = price;
        cartTempEntity.avalability = availability;
        insertAsyncTask(cartTempEntity);
    }


    public static void insertAsyncTask(final CartTempEntity cartTempEntity) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                cartTempRoomDatabase.cartTempDAO().insertTask(cartTempEntity);
                return null;
            }
        }.execute();
    }

    public void deleteItem(final int id) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                cartTempRoomDatabase.cartTempDAO().deleteItem(id);
                return null;
            }
        }.execute();
    }
    public void deleteAll() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                cartTempRoomDatabase.cartTempDAO().deleteAll();
                return null;
            }
        }.execute();
    }
    public LiveData<List<CartTempEntity>> getAllcartTempEntity() {
        return cartTempRoomDatabase.cartTempDAO().getAllCartTempEntity();
    }

    public LiveData<List<String>> getItemEntity() {
        return cartTempRoomDatabase.cartTempDAO().getItemEntity();
    }

}
