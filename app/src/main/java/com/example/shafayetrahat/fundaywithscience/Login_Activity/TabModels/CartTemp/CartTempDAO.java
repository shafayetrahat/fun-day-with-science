package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.CartTemp;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.ArrayList;
import java.util.List;

    @Dao
    public interface CartTempDAO {

        @Insert
        void insertTask(CartTempEntity cart );

        @Query("DELETE FROM cart")
        void deleteAll();

        @Query("DELETE FROM cart where id =:id")
        void deleteItem(int id);

        @Query("SELECT * from cart ORDER BY id ASC")
        LiveData<List<CartTempEntity>> getAllCartTempEntity();


        @Query("SELECT product from cart")
        LiveData<List<String>> getItemEntity();
    }


