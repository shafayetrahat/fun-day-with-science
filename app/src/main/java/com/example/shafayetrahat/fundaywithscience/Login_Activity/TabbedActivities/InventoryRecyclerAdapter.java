package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.shafayetrahat.fundaywithscience.R;

import java.util.List;

public class InventoryRecyclerAdapter extends RecyclerView.Adapter<InventoryRecyclerAdapter.ViewHolder> {

    private List<String> mData;
    private List<Integer> mPrice;
    private List<Integer> mAvailability;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;



    // data is passed into the constructor
    InventoryRecyclerAdapter(Context context, List<String> data,List<Integer> price,List<Integer> availabilty) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mPrice = price;
        this.mAvailability = availabilty;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.inventory_recycler_view, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String event = mData.get(position);
        Integer itemPrice = mPrice.get(position);
        Integer itemAvailability = mAvailability.get(position);
        holder.myTextView.setText(event);
        holder.myPrice.setText(itemPrice.toString()+" TK");
        holder.myAvailability.setText("Available  " +itemAvailability.toString());
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        TextView myPrice;
        TextView myAvailability;
        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.inventoryEventName);
            myPrice = itemView.findViewById(R.id.inventoryEventprice);
            myAvailability = itemView.findViewById(R.id.inventoryEventamount);
//            itemView.setOnClickListener(this);
            FloatingActionButton fab = (FloatingActionButton) itemView.findViewById(R.id.addcart);
            fab.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, getAdapterPosition());
        }
        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return mData.get(id);
    }
    Integer getprice(int id) {
        return mPrice.get(id);
    }
    Integer getavailability(int id) {
        return mAvailability.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}