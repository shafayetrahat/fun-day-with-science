package com.example.shafayetrahat.fundaywithscience.RouteInterface;

import com.example.shafayetrahat.fundaywithscience.Models.Award;
import com.example.shafayetrahat.fundaywithscience.Models.AwardBody;
import com.example.shafayetrahat.fundaywithscience.Models.CartBody;
import com.example.shafayetrahat.fundaywithscience.Models.Events;
import com.example.shafayetrahat.fundaywithscience.Models.InventoryModel;
import com.example.shafayetrahat.fundaywithscience.Models.Login;
import com.example.shafayetrahat.fundaywithscience.Models.Login_Body;
import com.example.shafayetrahat.fundaywithscience.Models.Profile;
import com.example.shafayetrahat.fundaywithscience.Models.Profile_body;
import com.example.shafayetrahat.fundaywithscience.Models.Share_Coins;
import com.example.shafayetrahat.fundaywithscience.Models.ShareCoinsBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RouteInterface {
        @POST("/api/login")
        Call<Login> userLogin(@Body Login_Body userLoginCredential);

        @Headers({ "Content-Type: application/json;charset=UTF-8"})
        @POST("api/see_profile")
        Call<Profile> getUserProfile(@Header("Authorization") String auth, @Body Profile_body profile_body);

        @Headers({ "Content-Type: application/json;charset=UTF-8"})
        @POST("api/share_coins_submit")
        Call<Share_Coins> transferCoins(@Header("Authorization") String auth, @Body ShareCoinsBody shareCoinsBody);

        @Headers({ "Content-Type: application/json;charset=UTF-8"})
        @POST("api/events")
        Call<Events> getEventList(@Header("Authorization") String auth);

        @Headers({ "Content-Type: application/json;charset=UTF-8"})
        @POST("api/confirm_award")
        Call<Award> postAward(@Header("Authorization") String auth, @Body AwardBody awardBody);

        @Headers({ "Content-Type: application/json;charset=UTF-8"})
        @POST("api/inventory")
        Call<InventoryModel> getInventoryList(@Header("Authorization") String auth);

        @Headers({ "Content-Type: application/json;charset=UTF-8"})
        @POST("api/confirm_purchase")
        Call<Integer> postPurchase(@Header("Authorization") String auth, @Body CartBody cartBody);

}
