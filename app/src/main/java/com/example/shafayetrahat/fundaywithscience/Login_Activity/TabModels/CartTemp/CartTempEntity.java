package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.CartTemp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity(tableName = "cart")
public class CartTempEntity {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "product")
    public String product;

    @ColumnInfo(name = "price")
    public Integer price;

    @ColumnInfo(name = "availability")
    public Integer avalability;


}

