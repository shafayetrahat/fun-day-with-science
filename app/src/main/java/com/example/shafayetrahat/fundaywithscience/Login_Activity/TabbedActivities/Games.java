package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.shafayetrahat.fundaywithscience.Models.Event;
import com.example.shafayetrahat.fundaywithscience.Models.Events;
import com.example.shafayetrahat.fundaywithscience.Network.Network;
import com.example.shafayetrahat.fundaywithscience.R;
import com.example.shafayetrahat.fundaywithscience.RouteInterface.RouteInterface;
import com.example.shafayetrahat.fundaywithscience.Shared_Preferences;
import com.example.shafayetrahat.fundaywithscience.barcodeUtil.BarcodeCaptureActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Games extends AppCompatActivity implements RecyclerAdapter.ItemClickListener{
    RecyclerAdapter adapter;
    private RouteInterface routeEvents;
    private ProgressBar pgsBar;

    ArrayList<String> EventName = new ArrayList<>();
    ArrayList<String> EventDetails = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games);
        pgsBar = (ProgressBar) findViewById(R.id.progressBar1);

        // set up the RecyclerView
        pgsBar.setVisibility(View.VISIBLE);
        eventList();
        try {
            Thread.sleep(500);
            pgsBar.setVisibility(View.GONE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        RecyclerView recyclerView = findViewById(R.id.events);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        adapter = new RecyclerAdapter(this, EventName, EventDetails);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        Shared_Preferences.setSharedPreferenceInt(Games.this,"event_id",position+1);
        Intent intent = new Intent(Games.this, GamesConfirm.class);
        startActivity(intent);
    }
    public void eventList(){
        routeEvents = Network.getClient().create(RouteInterface.class);
        String token = Shared_Preferences.getSharedPreferenceString(this,"token","");
        Call<Events> call = routeEvents.getEventList("Bearer "+token);
        call.enqueue(new Callback<Events>() {
            Events events = null;
            @Override
            public void onResponse(Call<Events> call, Response<Events> response) {
                try {
                    events = response.body();
                    for(Event event : events.getEvents()) {
                        EventName.add(event.getEventName());
                        EventDetails.add(event.getEventDescription());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<Events> call, Throwable t) {
                Toast.makeText(Games.this,"Error in network",Toast.LENGTH_LONG).show();

            }

        });
    }


}
