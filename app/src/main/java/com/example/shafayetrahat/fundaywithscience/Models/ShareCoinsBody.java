package com.example.shafayetrahat.fundaywithscience.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShareCoinsBody {
    @SerializedName("reciever_qr_code")
    @Expose
    private String recieverQrCode;
    @SerializedName("sender_qr_code")
    @Expose
    private String senderQrCode;
    @SerializedName("amount")
    @Expose
    private Integer amount;

    public String getSenderQrCode() {
        return senderQrCode;
    }

    public void setSenderQrCode(String senderQrCode) {
        this.senderQrCode = senderQrCode;
    }

    public String getRecieverQrCode() {
        return recieverQrCode;
    }

    public void setRecieverQrCode(String recieverQrCode) {
        this.recieverQrCode = recieverQrCode;
    }

    public Integer getamount() {
        return amount;
    }

    public void setamount(Integer amount) {
        this.amount = amount;
    }

}
