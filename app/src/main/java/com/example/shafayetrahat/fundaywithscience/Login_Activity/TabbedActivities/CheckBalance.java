package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shafayetrahat.fundaywithscience.Login_Activity.Panel_Page;
import com.example.shafayetrahat.fundaywithscience.Models.Profile;
import com.example.shafayetrahat.fundaywithscience.Models.Profile_body;
import com.example.shafayetrahat.fundaywithscience.Network.Network;
import com.example.shafayetrahat.fundaywithscience.R;
import com.example.shafayetrahat.fundaywithscience.RouteInterface.RouteInterface;
import com.example.shafayetrahat.fundaywithscience.Shared_Preferences;
import com.example.shafayetrahat.fundaywithscience.barcodeUtil.BarcodeCaptureActivity;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckBalance extends AppCompatActivity {
    TextView result;
    private RouteInterface route_profile;
    public static final int RC_BARCODE_CAPTURE = 9001;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_balance);
//        String token = Shared_Preferences.getSharedPreferenceString(this,"token","");
//        Toast.makeText(CheckBalance.this,token,Toast.LENGTH_LONG).show();
        Button Scan = findViewById(R.id.scan);
        Scan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showQRScanner();
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CommonStatusCodes.SUCCESS && requestCode == RC_BARCODE_CAPTURE) {
            if (data == null) return;
            Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
            final String scanResult = barcode.displayValue;
            if (scanResult == null) return;
            result = findViewById(R.id.qr_token);
            Profile_body profilebody = new Profile_body();
            profilebody.setQrCode(scanResult);
            showProfile(profilebody);


        }
    }
    public void showQRScanner() {
        Intent intent = new Intent(CheckBalance.this, BarcodeCaptureActivity.class);
        startActivityForResult(intent, RC_BARCODE_CAPTURE);
        return;
    }
    public void showProfile(Profile_body profile_body){
        route_profile = Network.getClient().create(RouteInterface.class);
        String token = Shared_Preferences.getSharedPreferenceString(this,"token","");
        Call<Profile> call = route_profile.getUserProfile("Bearer "+token, profile_body);
        Profile details = null;
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                Profile details = response.body();
                String detailsUser = details.getUser();
                if(detailsUser!=null){
                    try {
                        result.setText("Hello "+detailsUser+"\n "
                                + "You have net "+details.getNetCoin()+" coins\n"
                                + "You earned "+details.getEarnCoin()+" coins\n"
                                + "You spent "+details.getSpentCoin()+"coins");
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                Toast.makeText(CheckBalance.this,"Error in network",Toast.LENGTH_LONG).show();
            }
        });
    }
}
