package com.example.shafayetrahat.fundaywithscience.Login_Activity;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.CartTemp.CartRepo;
import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.CartTemp.CartTempEntity;
import com.example.shafayetrahat.fundaywithscience.Login_Activity.TabbedActivities.InventoryRecyclerAdapter;
import com.example.shafayetrahat.fundaywithscience.Models.CartBody;
import com.example.shafayetrahat.fundaywithscience.Network.Network;
import com.example.shafayetrahat.fundaywithscience.R;
import com.example.shafayetrahat.fundaywithscience.RouteInterface.RouteInterface;
import com.example.shafayetrahat.fundaywithscience.Shared_Preferences;
//import com.example.shafayetrahat.fundaywithscience.RouteInterface.RouteInterface;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Cart_Page extends AppCompatActivity implements CartRecyclerAdapter.ItemClickListener{
    CartRecyclerAdapter adapter;
//    private RouteInterface routeInventory;
    //    private ProgressBar pgsBar;
    String qrCode = null;
    ArrayList<String> itemName = new ArrayList<>();
    ArrayList<Integer> itemPrice = new ArrayList<>();
    ArrayList<Integer> itemAmount = new ArrayList<>();
    ArrayList<Integer> tempAmount = new ArrayList<>();
    ArrayList<Integer> ids = new ArrayList<>();
    int netPrice;
    private RouteInterface route_profile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart__page);
        cartList();
        RecyclerView recyclerView = findViewById(R.id.cart);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        adapter = new CartRecyclerAdapter(this,itemName, itemPrice, tempAmount);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT ){

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                final int position = viewHolder.getAdapterPosition(); //swiped position

                if (direction == ItemTouchHelper.LEFT) { //swipe left

                    CartRepo cartRepo = new CartRepo(getApplicationContext());
                    cartRepo.deleteItem(ids.get(position));
                    itemName.remove(position);
                    itemPrice.remove(position);
                    tempAmount.remove(position);
                    ids.remove(position);
                    adapter.notifyItemRemoved(position);
                    onChange();


                }else if(direction == ItemTouchHelper.RIGHT){//swipe right

                    CartRepo cartRepo = new CartRepo(getApplicationContext());
                    cartRepo.deleteItem(ids.get(position));
                    itemName.remove(position);
                    itemPrice.remove(position);
                    tempAmount.remove(position);
                    ids.remove(position);

                    adapter.notifyItemRemoved(position);
                    onChange();

                }

            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);


            qrCode= Shared_Preferences.getSharedPreferenceString(this,"qrCode","");
        Button confirm = findViewById(R.id.confirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CartBody cartBody = new CartBody();
                cartBody.setAmounts(tempAmount);
                cartBody.setItemName(itemName);
                cartBody.setNetPrice(netPrice);
                cartBody.setQrCode(qrCode);
                purchaseRoute(cartBody);
            }
        });
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    public void cartList(){
                try {
                    final CartRepo cartRepo = new CartRepo(getApplicationContext());
                    cartRepo.getAllcartTempEntity().observe(Cart_Page.this, new Observer<List<CartTempEntity>>() {

                        @Override
                        public void onChanged(List<CartTempEntity> QRTempEntities) {
//                            int pos=0;
                            for (CartTempEntity cartTempEntity : QRTempEntities) {

//                                System.out.println("#############" + cartTempEntity);
                                itemName.add(cartTempEntity.product);
                                itemPrice.add(cartTempEntity.price);
                                itemAmount.add(cartTempEntity.avalability);
                                ids.add(cartTempEntity.id);
//                                System.out.println("##########position" + itemName.get(pos));
//                                pos++;
                            }
                            int cartSize = itemName.size();
                            netPrice=0;
                            for(int i = 0; i < itemName.size(); i++) {
                                tempAmount.add(1);
                            }
                            for(int i=0; i<cartSize; i++){
                                netPrice = adapter.getprice(i)*adapter.tempAmount.get(i) + netPrice;

                            }
                            TextView totalTag = findViewById(R.id.totalTag);
                            totalTag.setText("Total Price");
                            TextView total = findViewById(R.id.totalPrice);
                            total.setText(String.valueOf(netPrice));
                        }});

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

    public void onChange() {
            int cartSize = adapter.tempAmount.size();
            int netPrice = 0;
            for (int i = 0; i < cartSize; i++) {
                netPrice = adapter.getprice(i) * adapter.tempAmount.get(i) + netPrice;
            }
            TextView total = findViewById(R.id.totalPrice);
            total.setText(String.valueOf(netPrice) + "TK ");

    }

    @Override
    public void onIncreaseClick(View view, int position) {
        int maxItem = itemAmount.get(position);
        if (adapter.tempAmount.get(position)<maxItem) {
            int tempCount = adapter.tempAmount.get(position);
            tempCount = tempCount + 1;
            tempAmount.set(position, tempCount);
            adapter.tempAmount.set(position, tempCount);
            adapter.notifyItemChanged(position);
            int cartSize = adapter.tempAmount.size();
            int netPrice = 0;
            for (int i = 0; i < cartSize; i++) {
                netPrice = adapter.getprice(i) * adapter.tempAmount.get(i) + netPrice;
                System.out.println(tempAmount.get(i)+" !!! ");
            }
            TextView total = findViewById(R.id.totalPrice);
            total.setText(String.valueOf(netPrice) + "TK ");
        }
        else {
                    Toast.makeText(Cart_Page.this,adapter.getItem(position) + " is maximum. Can't get more of this item", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onDecreaseClick(View view, int position) {

        if(adapter.tempAmount.get(position)>0 ) {
            int tempCount = adapter.tempAmount.get(position);
            tempCount = tempCount - 1;
            tempAmount.set(position, tempCount);
            adapter.tempAmount.set(position, tempCount);
            adapter.notifyItemChanged(position);
            int cartSize = adapter.tempAmount.size();
            int netPrice = 0;
            for (int i = 0; i < cartSize; i++) {
                netPrice = adapter.getprice(i) * adapter.tempAmount.get(i) + netPrice;
            }
            TextView total = findViewById(R.id.totalPrice);
            total.setText(String.valueOf(netPrice) + "TK  ");
        }
        else {
                    Toast.makeText(Cart_Page.this, adapter.getItem(position)+" is minimum " , Toast.LENGTH_SHORT).show();
        }
    }


    public void purchaseRoute(CartBody cartBody){
        route_profile = Network.getClient().create(RouteInterface.class);
        String token = Shared_Preferences.getSharedPreferenceString(this,"token","");
        Call<Integer> call = route_profile.postPurchase("Bearer "+token, cartBody);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                Integer status = response.body();
                if(status!=null){
                    if (status == 200) {
                        Toast.makeText(Cart_Page.this, "Purchase Successful", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(Cart_Page.this,"Purchase failed",Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Toast.makeText(Cart_Page.this,"Error in network",Toast.LENGTH_LONG).show();

            }
        });
        Shared_Preferences.deleteSharedPreference(this, "qrCode");
        CartRepo cartRepo = new CartRepo(this);
        cartRepo.deleteAll();
        Intent k = new Intent(Cart_Page.this, Panel_Page.class);
        startActivity(k);

    }
}


