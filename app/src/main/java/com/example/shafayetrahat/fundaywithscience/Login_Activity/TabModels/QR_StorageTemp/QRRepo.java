package com.example.shafayetrahat.fundaywithscience.Login_Activity.TabModels.QR_StorageTemp;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;


public class QRRepo {
    private String DB_NAME = "TempDatabase";

    private static QRTempRoomDatabase qrTempRoomDatabase;
    public QRRepo(Context context) {
        qrTempRoomDatabase = Room.databaseBuilder(context, QRTempRoomDatabase.class, DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    public void insertTask(String qrCode) {

        QRTempEntity qrTempEntity = new QRTempEntity();
        qrTempEntity.qrCode = qrCode;
        insertAsyncTask(qrTempEntity);
    }


    public static void insertAsyncTask(final QRTempEntity qrTempEntity) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                qrTempRoomDatabase.qrTempDAO().insertTask(qrTempEntity);
                return null;
            }
        }.execute();
    }
    public static void deleteAll(){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                qrTempRoomDatabase.qrTempDAO().deleteAll();
                return null;
            }
        }.execute();
    }
    public LiveData<List<QRTempEntity>> getAllqrTempEntity() {
        return qrTempRoomDatabase.qrTempDAO().getAllQRTempEntity();
    }


}
