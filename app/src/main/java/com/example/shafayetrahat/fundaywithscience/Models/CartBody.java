package com.example.shafayetrahat.fundaywithscience.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartBody {

    @SerializedName("amounts")
    @Expose
    private List<Integer> amounts = null;
    @SerializedName("net_price")
    @Expose
    private Integer netPrice;
    @SerializedName("item_name")
    @Expose
    private List<String> itemName = null;
    @SerializedName("qr_code")
    @Expose
    private String qrCode;

    public List<Integer> getAmounts() {
        return amounts;
    }

    public void setAmounts(List<Integer> amounts) {
        this.amounts = amounts;
    }

    public Integer getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(Integer netPrice) {
        this.netPrice = netPrice;
    }

    public List<String> getItemName() {
        return itemName;
    }

    public void setItemName(List<String> itemName) {
        this.itemName = itemName;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

}